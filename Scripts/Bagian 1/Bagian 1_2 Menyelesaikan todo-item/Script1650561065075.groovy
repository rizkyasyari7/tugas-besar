import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://todomvc.com/examples/knockoutjs/')

WebUI.setText(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/input_todos_new-todo'), 'Belajar')

WebUI.sendKeys(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/input_todos_new-todo'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/input_todos_new-todo'), 'Mengaji')

WebUI.sendKeys(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/input_todos_new-todo'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/input_todos_new-todo'), 'Sekolah')

WebUI.sendKeys(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/input_todos_new-todo'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/input_todos_new-todo'), 'Makan')

WebUI.sendKeys(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/input_todos_new-todo'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/input_Mark all as complete_toggle'))

WebUI.click(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/input_Mark all as complete_toggle - Copy'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/label_Belajar'), 'Belajar')

WebUI.verifyElementText(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/label_Mengaji'), 'Mengaji')

WebUI.verifyElementText(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/label_Sekolah'), 'Sekolah')

WebUI.verifyElementText(findTestObject('Object Repository/Page_Knockout.js  TodoMVC/label_Makan'), 'Makan')

WebUI.closeBrowser()


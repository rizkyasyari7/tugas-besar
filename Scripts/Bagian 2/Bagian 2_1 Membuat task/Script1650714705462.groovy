import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\ASUS\\Downloads\\org.tasks_120603.apk', true)

Mobile.tap(findTestObject('Object Repository/My Task/android.widget.ImageButton Tambah Task'), 0)

Mobile.setText(findTestObject('Object Repository/My Task/android.widget.EditText - Task name'), 'Tugas 1', 0)

Mobile.tap(findTestObject('My Task/android.view.View Dismiss'), 0)

Mobile.tap(findTestObject('My Task/android.widget.LinearLayout Start Date'), 0)

Mobile.tap(findTestObject('Object Repository/My Task/android.widget.CompoundButton - 8 PM'), 0)

Mobile.tap(findTestObject('My Task/android.widget.Button - OK'), 0)

Mobile.tap(findTestObject('My Task/android.widget.LinearLayout Due Date'), 0)

Mobile.tap(findTestObject('Object Repository/My Task/android.widget.CompoundButton - Tomorrow'), 0)

Mobile.tap(findTestObject('My Task/android.widget.Button - OK'), 0)

Mobile.tap(findTestObject('Object Repository/My Task/android.widget.RadioButton'), 0)

Mobile.tap(findTestObject('Object Repository/My Task/android.widget.TextView - Add subtask'), 0)

Mobile.setText(findTestObject('Object Repository/My Task/android.widget.EditText - Enter title'), 'Task 1_1', 0)

Mobile.tap(findTestObject('Object Repository/My Task/android.widget.ImageButton Simpan Task'), 0)

Mobile.verifyElementVisible(findTestObject('My Task/android.widget.TextView - Tugas 1'), 0)

Mobile.verifyElementVisible(findTestObject('My Task/android.widget.TextView - Task 1_1'), 0)

Mobile.verifyElementVisible(findTestObject('My Task/android.widget.Button - 8 PM'), 0)

Mobile.closeApplication()


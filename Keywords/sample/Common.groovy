package sample

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import groovy.json.JsonSlurper

import internal.GlobalVariable

public class Common {
	public static JsonSlurper jsonSlurper = new JsonSlurper()

	@Keyword
	def int createNewUser(String nama, String surel, String kota, int expectedStatus) {
		def response = WS.sendRequestAndVerify(findTestObject("Object Repository/Bagian 3/POST User",
				["nama": nama, "surel": surel, "kota": kota]))

		def jsonResponse = jsonSlurper.parseText(response.getResponseText())
		return jsonResponse.id
	}

	@Keyword
	def static void findUserById(int id, String nama, String surel, String kota, int expectedStatus) {
		WS.sendRequestAndVerify(findTestObject('Object Repository/Bagian 3/GET Specific User', [('id') : id]))
	}
}

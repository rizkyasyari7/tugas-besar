<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Tes_destroy</name>
   <tag></tag>
   <elementGuidId>584c68d7-ac36-4ecd-9e7d-78c2d231dcfe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/section/section/ul/li/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.destroy</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>017f292f-310e-4326-9756-8a04cbf4e9a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>destroy</value>
      <webElementGuid>cbf63b6c-de89-4776-9415-8e47a6f3b07f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>click: $root.remove</value>
      <webElementGuid>7da965a3-d0a5-49e3-b642-0308d6ef2389</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;learn-bar&quot;]/section[@class=&quot;todoapp&quot;]/section[@class=&quot;main&quot;]/ul[@class=&quot;todo-list&quot;]/li[1]/div[@class=&quot;view&quot;]/button[@class=&quot;destroy&quot;]</value>
      <webElementGuid>8ccd7853-f1f5-4b2e-9d79-accda940c516</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>/html/body/section/section/ul/li/div/button</value>
      <webElementGuid>bf750220-5b2a-495b-ba5a-888667aaa5c9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

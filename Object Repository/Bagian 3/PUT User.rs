<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>PUT User</name>
   <tag></tag>
   <elementGuidId>c3dab5dc-56c2-43fc-b48f-887aaa5a71d2</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;nama\&quot;: \&quot;Rahma Fitri Asriani\&quot;,\n    \&quot;surel\&quot;: \&quot;rahma@sv.it\&quot;,\n  \t\&quot;kota\&quot;: \&quot;pekanbaru\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>6b01dd12-d771-4f02-a404-ba99d1b0e2da</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.3.0</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>https://reqres.in/api/users/7</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'Rahma Fitri Asriani'</defaultValue>
      <description></description>
      <id>17c28e32-f5e6-4cda-b474-478d83b0ebf7</id>
      <masked>false</masked>
      <name>nama</name>
   </variables>
   <variables>
      <defaultValue>'rahma@sv.it'</defaultValue>
      <description></description>
      <id>c5654889-a16a-47bb-8a82-b99ad72dbe48</id>
      <masked>false</masked>
      <name>surel</name>
   </variables>
   <variables>
      <defaultValue>'Pekanbaru'</defaultValue>
      <description></description>
      <id>dbd98145-5cef-4697-93f0-697ac95d9b53</id>
      <masked>false</masked>
      <name>kota</name>
   </variables>
   <variables>
      <defaultValue>7</defaultValue>
      <description></description>
      <id>43a59431-e6b4-46da-9631-87747c1f4edd</id>
      <masked>false</masked>
      <name>id</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
WS.verifyElementPropertyValue(response, 'nama', &quot;Rahma Fitri Asriani&quot;)
WS.verifyElementPropertyValue(response, 'surel', &quot;rahma@sv.it&quot;)
WS.verifyElementPropertyValue(response, 'kota', &quot;pekanbaru&quot;)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
